from netmiko import Netmiko
import difflib
import webbrowser
from datetime import datetime
import argparse

# Import config or give useful error if it doesn't exist.
try:
    from config import *
except ModuleNotFoundError:
    print("Could not import config. You must copy 'example-config.py' to 'config.py' and edit the variables for your environment.")
    exit()

# Setup argument parsing
#parser = parser = argparse.ArgumentParser(description='Run once in --pre mode, then again as many times with --compare')
#parser.add_argument('-p', '--pre', help='Creates the initial "pre-maintenance.txt" output file and all subsequent runs compare against. Always run the script with this first.')
#parser.add_argument('-c', '--compare', help='Compare against "pre-maintenance.txt". Will output to stdout, text file, and HTML.')
#parser.add_argument('-f', '--commandfile', help='Specify file with list of commands to run. Default is "commands.txt"')
#parser.add_argument('-C', '--configfile', help='Override default config file (default is "config.py"')
#args = parser.parse_args()


def set_script_time():
    script_time = datetime.now().isoformat()
    return script_time

def select_mode():
    try:
        with open('pre-maintenance.txt') as infile:
            pre = infile.readlines()
        mode = 'compare'
    except:
        mode = 'first_run'
    return mode

def write_command_list(output_name, commands_file='commands.txt'):
    with open(commands_file) as infile:
        commands = infile.readlines()

    with open(output_name, 'a',) as outfile:
        net_connect = Netmiko(host=host_name, username=user_name, password=password, device_type=device_type)
        for command in commands:
            output = net_connect.send_command(command)
            outfile.write(f'\n\n#### {command.strip()} ####\n')
            outfile.write(output)
            outfile.write('\n')
        net_connect.disconnect()

def compare_configs(script_time):
    with open('pre-maintenance.txt') as infile:
        pre = infile.readlines()

    with open(f'{script_time}_output.txt') as infile:
        post = infile.readlines()

    for line in difflib.unified_diff(pre,post):
        print(line)

    hdiff = difflib.HtmlDiff()

    with open(f'{script_time}_diff.html', 'w') as outfile:
        outfile.write(hdiff.make_file(pre, post))
        html_filename = outfile.name
        print(f'#####################################')
        print(f'Saved HTML diff to {html_filename}')

    print(f'Attempting to open {html_filename} in browser...')
    try:
        webbrowser.open(html_filename)
    except:
        print(f'Opening file in browser failed. Please open {html_filename} manually from the same directory the script was run.')

def main():
    ## Setup


    script_time = set_script_time()
    script_mode = select_mode()

    if script_mode == 'first_run':
        write_command_list('pre-maintenance.txt')
        with open('pre-maintenance.txt') as infile:
            for line in infile.readlines():
                print(line)
        print(f'#####################################'
              f'Output save to pre-maintenance.txt...'
              f'#####################################')
    elif script_mode == 'compare':
        write_command_list(f'{script_time}_output.txt')
        compare_configs(script_time)

if __name__ == "__main__":
    main()
